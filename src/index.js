import './style.css';

import initAuthorization from './handlers/authorization';
import initForm from './handlers/form';

document.addEventListener('DOMContentLoaded', async () => {
  initAuthorization();
  initForm();
});
