class Base {
  constructor(query) {
    if (!query) throw new Error('query is undefined!');

    this.el = document.querySelector(query);
  }
}

export default Base;
