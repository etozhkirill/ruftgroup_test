import Base from './Base';

class Result extends Base {
  constructor(query) {
    super(query);

    this.totalEl = document.createElement('div');
    this.totalEl.classList.add('result__total');

    this.listEl = document.createElement('div');
    this.listEl.classList.add('result__list', 'list');
  }

  renderResult({ count, items }) {
    this.el.innerHTML = '';

    this.totalEl.innerText = `Найдено: ${count || 0} пользователей`;
    this.el.appendChild(this.totalEl);

    this.listEl.innerHTML = '';
    this.appendItems(items);
    this.el.appendChild(this.listEl);
  }

  appendItems(items) {
    items.forEach((item) => {
      const listItemEl = document.createElement('div');
      listItemEl.classList.add('list__item');
      listItemEl.innerHTML = `
        <div class="card">
          <div class="card__image">
            <img src="${item.photo_200}" data-loading>
          </div>
          <a class="card__link" href="https://vk.com/id${item.id}" target="_blank">
            ${item.first_name} ${item.last_name}
          </a>
        </div>
      `;

      // init image lazyloading
      const img = listItemEl.querySelector('[data-loading]');
      img.onload = () => img.removeAttribute('data-loading');

      this.listEl.appendChild(listItemEl);
    });
  }
}

export default Result;
