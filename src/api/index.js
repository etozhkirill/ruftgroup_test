import jsonp from '../utils/jsonp';

class Api {
  constructor(accessToken) {
    this.accessToken = accessToken;
  }

  method(methodName, params) {
    const paramsString = Object.keys(params).reduce((acc, paramKey) => {
      let result = acc;
      const val = params[paramKey];

      if (val) result += `&${paramKey}=${val}`;

      return result;
    }, '');
    const url = `https://api.vk.com/method/${methodName}?access_token=${this.accessToken}&v=5.101${paramsString}`;

    return jsonp(url);
  }
}

export default Api;
