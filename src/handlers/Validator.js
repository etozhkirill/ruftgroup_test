class Validator {
  constructor(form) {
    this.form = form;

    Object.keys(this.form).forEach((key) => {
      const item = this.form[key];
      const { el } = item;
      item.isValid = false;

      if (!el) return;

      // TODO: add debounce
      el.addEventListener('input', () => (this.checkValidation(key)));
    });
  }

  checkValidation(key) {
    const item = this.form[key];
    const value = item.el.value.trim();

    const isInvalid = (item.regex && !item.regex.test(value))
      || ((item.min !== undefined) && (parseInt(value, 10) < item.min))
      || ((item.max !== undefined) && (parseInt(value, 10) > item.max));

    if (isInvalid) {
      item.isValid = false;
      item.el.classList.add('input_error');
      return;
    }

    item.isValid = true;
    item.el.classList.remove('input_error');
  }

  validate() {
    Object.keys(this.form).forEach((key) => {
      const item = this.form[key];
      const { el } = item;
      item.isValid = false;

      if (!el) return;

      this.checkValidation(key);
    });
  }

  get isValid() {
    return !Object.keys(this.form).find((key) => !this.form[key].isValid);
  }
}

export default Validator;
