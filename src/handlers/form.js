import Api from '../api';
import { calculateAge } from '../utils';
import { getCookie } from '../utils/cookies';
import Form from '../components/Form';
import Result from '../components/Result';
import Validator from './Validator';
import Error from '../components/Error';

const count = 20;
let offset = 0;

export default () => {
  const accessToken = getCookie('accessToken');
  const api = new Api(accessToken);

  const dayEl = document.getElementById('day');
  const monthEl = document.getElementById('month');
  const yearEl = document.getElementById('year');

  const formComponent = new Form('#search');
  const resultComponent = new Result('.result');
  const errorComponent = new Error('.error');

  const validator = new Validator({
    day: {
      el: dayEl,
      min: 1,
      max: 31,
      regex: /^\d+$/,
    },
    month: {
      el: monthEl,
      min: 1,
      max: 12,
      regex: /^\d+$/,
    },
    year: {
      el: yearEl,
      max: new Date().getFullYear(),
      regex: /^\d+$/,
    },
  });

  const observer = new IntersectionObserver(async ([entry]) => {
    if (!entry.isIntersecting) return;

    offset += count;

    const { response } = await api.method('users.search', {
      fields: 'photo_200,first_name,last_name',
      birth_day: dayEl.value,
      birth_month: monthEl.value,
      birth_year: yearEl.value,
      offset,
      count,
    });

    resultComponent.appendItems(response.items);

    if (offset >= response.count) observer.disconnect();
  });

  formComponent.submit(async () => {
    validator.validate();

    if (!validator.isValid) return;

    const age = calculateAge(new Date(yearEl.value, monthEl.value, dayEl.value));
    if ((age < 20) || (age > 40)) {
      errorComponent.show('Возраст должен быть не менее 20 и не более 40 лет');
      return;
    }

    errorComponent.hide();

    offset = 0;

    try {
      const { response } = await api.method('users.search', {
        fields: 'photo_200,first_name,last_name',
        birth_day: dayEl.value,
        birth_month: monthEl.value,
        birth_year: yearEl.value,
        count,
      });

      resultComponent.renderResult(response);

      observer.observe(document.querySelector('.wrapper__observer'));
    } catch (err) {
      console.error(err);
    }
  });
};
