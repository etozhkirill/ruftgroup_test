import { hash2obj } from '../utils';
import { getCookie, setCookie } from '../utils/cookies';
import config from '../config';

const authorizeUrl = `https://oauth.vk.com/authorize?client_id=${config.clientId}
  &redirect_uri=${config.redirectUrl}&display=page&response_type=token`;

export default () => {
  const hash = hash2obj(window.location.hash);
  const accessToken = getCookie('accessToken');

  if (hash.access_token) {
    setCookie('accessToken', hash.access_token, {
      expires: new Date(Date.now() + hash.expires_in * 1000).toUTCString(),
    });
    window.history.replaceState({}, '', '/');

    return;
  }

  if (!accessToken) {
    window.location.href = authorizeUrl;
  }
};
